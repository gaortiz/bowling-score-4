package com.jobsity.challenge.model;

public class Shot {

    private String name;
    private Integer value;

    public Shot(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
