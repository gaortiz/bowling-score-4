package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.exceptions.BowlingScoreException;
import com.jobsity.challenge.exceptions.InvalidFramesException;
import com.jobsity.challenge.exceptions.InvalidScoreException;
import com.jobsity.challenge.factories.IFrameResultCreator;
import com.jobsity.challenge.factories.ILastFrameCreator;
import com.jobsity.challenge.factories.IShotCreator;
import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.service.ILastFrameHandlerService;
import com.jobsity.challenge.service.IScoreCalculator;
import com.jobsity.challenge.service.IValidator;
import com.jobsity.challenge.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ScoreCalculator implements IScoreCalculator {

    private static final String F = "F";
    private static final String ZERO_STRING = "0";
    private static final int CONTINUE_ITERATION_WHEN_NOT_STRIKE = 2;

    private ILastFrameHandlerService lastFrameHandlerService;
    private IValidator validator;
    private IShotCreator shotCreator;
    private ILastFrameCreator lastFrameCreator;
    private IFrameResultCreator frameResultCreator;

    @Autowired
    public void setLastFrameHandlerService(ILastFrameHandlerService lastFrameHandlerService) {
        this.lastFrameHandlerService = lastFrameHandlerService;
    }

    @Autowired
    public void setValidator(IValidator validator) {
        this.validator = validator;
    }

    @Autowired
    public void setShotCreator(IShotCreator shotCreator) {
        this.shotCreator = shotCreator;
    }

    @Autowired
    public void setLastFrameCreator(ILastFrameCreator lastFrameCreator) {
        this.lastFrameCreator = lastFrameCreator;
    }

    @Autowired
    public void setFrameResultCreator(IFrameResultCreator frameResultCreator) {
        this.frameResultCreator = frameResultCreator;
    }

    private int convertShotToNumericValue(String shot) throws InvalidScoreException {
        validator.validateScore(shot);
        if (shot.equals(F)) {
            return 0;
        } else {
            return Integer.parseInt(shot);
        }
    }

    public FramesResult calculateScore(final List<String> shots, final String player) throws BowlingScoreException {
        final List<Integer> score = new ArrayList<>();
        final Map<Integer, List<String>> pinFalls = new HashMap<>();
        final int framesLength = shots.size();
        int sum = 0;
        int shotIndex = 0;
        int resultIndex = 1;
        try {
            do {
                List<String> pinFall;

                String actualShot = shots.get(shotIndex);
                String nextShot = shots.get(shotIndex + 1);
                String afterNextShot = shotIndex + 2 < framesLength ? shots.get(shotIndex + 2) : ZERO_STRING;

                int actualShotValue = convertShotToNumericValue(actualShot);
                int nextShotValue = convertShotToNumericValue(nextShot);
                int afterNextShotValue = convertShotToNumericValue(afterNextShot);

                boolean isFinalFrame = resultIndex == Constants.TOTAL_FRAMES;
                boolean isStrike = actualShotValue == Constants.MAX_PINFALL;
                boolean isSpare = actualShotValue + nextShotValue == Constants.MAX_PINFALL;

                final Frame frame = lastFrameCreator.createLastFrame(
                        shotCreator.createShot(actualShot, actualShotValue),
                        shotCreator.createShot(nextShot, nextShotValue),
                        shotCreator.createShot(afterNextShot, afterNextShotValue)
                );

                sum += actualShotValue + nextShotValue;

                if (isFinalFrame) {
                    pinFall = lastFrameHandlerService.setLastFramePinFalls(frame);
                    sum += afterNextShotValue;
                    shotIndex++;
                } else if (isStrike) {
                    pinFall = Collections.singletonList(Constants.X);
                    sum += afterNextShotValue;
                    shotIndex++;
                } else if (isSpare) {
                    pinFall = Arrays.asList(actualShot, Constants.SLASH);
                    sum += afterNextShotValue;
                    shotIndex += 2;
                } else {
                    pinFall = Arrays.asList(actualShot, nextShot);
                    shotIndex += 2;
                }

                pinFalls.put(resultIndex, pinFall);
                resultIndex++;
                score.add(sum);

            } while (resultIndex <= Constants.TOTAL_FRAMES);

            return frameResultCreator.createFrameResult(score, pinFalls, player);
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidFramesException();
        }
    }

}
