package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.factories.IInputScoreCreator;
import com.jobsity.challenge.model.InputScore;
import com.jobsity.challenge.service.IParserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Service
public class ParserDataService implements IParserDataService {

    private IInputScoreCreator inputScoreCreator;

    @Autowired
    public void setInputScoreCreator(IInputScoreCreator inputScoreCreator) {
        this.inputScoreCreator = inputScoreCreator;
    }

    private static final String WHITE_SPACE = " ";

    public Map<String, List<String>> getGroupedShots(Stream<String> lines) {
        return lines.map(line -> {
            final String name = line.split(WHITE_SPACE)[0];
            final String score = line.split(WHITE_SPACE)[1];
            return inputScoreCreator.createInputScore(name, score);
        }).collect(groupingBy(InputScore::getName, mapping(InputScore::getScore, toList())));
    }
}
