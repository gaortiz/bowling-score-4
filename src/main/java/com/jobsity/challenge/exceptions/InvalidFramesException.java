package com.jobsity.challenge.exceptions;

public class InvalidFramesException extends BowlingScoreException {

    public InvalidFramesException() {
        super("Frames should be 10 per player");
    }

}
