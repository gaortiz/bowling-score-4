package com.jobsity.challenge.controllers;

import com.jobsity.challenge.exceptions.BowlingScoreException;
import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.service.IParserDataService;
import com.jobsity.challenge.service.IPrinterService;
import com.jobsity.challenge.service.IScoreCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Controller
public class BowlingScoreController {

    private IParserDataService parserDataService;
    private IScoreCalculator scoreCalculator;
    private IPrinterService printerService;

    @Autowired
    public void setParserDataService(IParserDataService parserDataService) {
        this.parserDataService = parserDataService;
    }

    @Autowired
    public void setScoreCalculator(IScoreCalculator scoreCalculator) {
        this.scoreCalculator = scoreCalculator;
    }

    @Autowired
    public void setPrinterService(IPrinterService printerService) {
        this.printerService = printerService;
    }

    public void calculateScore(String fileName) {

        try {
            final Stream<String> streamLines = Files.lines(Paths.get(fileName));

            final Map<String, List<String>> groupedShots = parserDataService.getGroupedShots(streamLines);

            final List<FramesResult> framesResults = new ArrayList<>();

            for (Map.Entry<String, List<String>> entry : groupedShots.entrySet()) {
                framesResults.add(scoreCalculator.calculateScore(entry.getValue(), entry.getKey()));
            }

            printerService.printResults(framesResults);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("There was an error reading the file, please fix it or try it with another");
        } catch (BowlingScoreException e) {
            System.out.print(e.getMessage());
        }
    }

}
