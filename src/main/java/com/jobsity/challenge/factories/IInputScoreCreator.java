package com.jobsity.challenge.factories;

import com.jobsity.challenge.model.InputScore;

public interface IInputScoreCreator {

    InputScore createInputScore(String name, String score);
}
