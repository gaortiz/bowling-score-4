package com.jobsity.challenge.factories;

import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.model.Shot;

public interface ILastFrameCreator {

    Frame createLastFrame(Shot actual, Shot next, Shot afterNext);

}
