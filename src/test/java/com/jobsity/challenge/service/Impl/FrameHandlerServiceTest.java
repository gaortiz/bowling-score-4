package com.jobsity.challenge.service.Impl;

import com.jobsity.challenge.model.Frame;
import com.jobsity.challenge.model.Shot;
import com.jobsity.challenge.service.impl.LastFrameHandlerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FrameHandlerServiceTest {

    @InjectMocks
    private LastFrameHandlerService lastFrameHandlerService;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void setLastFramePinFalls_allStrikes_returnPerfectScore() {


        String actualShot = "X";
        String nextShot = "X";
        String afterNextShot = "X";
        int actualShotValue = 10;
        int nextShotValue = 10;
        int afterNextShotValue = 10;

        Frame lastFrame = new Frame(
                new Shot(actualShot, actualShotValue),
                new Shot(nextShot, nextShotValue),
                new Shot(afterNextShot, afterNextShotValue)
        );

        final List<String> expectedLastFramePinFall = Arrays.asList("X", "X", "X");

        List<String> pinFalls = lastFrameHandlerService.setLastFramePinFalls(lastFrame);

        Assert.assertEquals(expectedLastFramePinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_twoPinFallsLessThan10_returnTwoScores() {

        String actualShot = "1";
        String nextShot = "8";
        String afterNextShot = "0";
        int actualShotValue = 1;
        int nextShotValue = 8;
        int afterNextShotValue = 0;

        Frame lastFrame = new Frame(
                new Shot(actualShot, actualShotValue),
                new Shot(nextShot, nextShotValue),
                new Shot(afterNextShot, afterNextShotValue)
        );

        final List<String> expectedLastFramePinFall = Arrays.asList("1", "8");

        List<String> pinFalls = lastFrameHandlerService.setLastFramePinFalls(lastFrame);

        Assert.assertEquals(expectedLastFramePinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_spareOnSecondAttempt_returnSlashInSecondAttempt() {

        String actualShot = "5";
        String nextShot = "5";
        String afterNextShot = "5";
        int actualShotValue = 5;
        int nextShotValue = 5;
        int afterNextShotValue = 5;

        Frame lastFrame = new Frame(
                new Shot(actualShot, actualShotValue),
                new Shot(nextShot, nextShotValue),
                new Shot(afterNextShot, afterNextShotValue)
        );

        final List<String> expectedLastFramePinFall = Arrays.asList("5", "/", "5");

        List<String> pinFalls = lastFrameHandlerService.setLastFramePinFalls(lastFrame);

        Assert.assertEquals(expectedLastFramePinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_spareOnLastAttempt_returnSlashInLastAttempt() {

        String actualShot = "X";
        String nextShot = "0";
        String afterNextShot = "X";
        int actualShotValue = 10;
        int nextShotValue = 0;
        int afterNextShotValue = 10;

        Frame lastFrame = new Frame(
                new Shot(actualShot, actualShotValue),
                new Shot(nextShot, nextShotValue),
                new Shot(afterNextShot, afterNextShotValue)
        );

        final List<String> expectedLastFramePinFall = Arrays.asList("X", "0", "/");

        List<String> pinFalls = lastFrameHandlerService.setLastFramePinFalls(lastFrame);

        Assert.assertEquals(expectedLastFramePinFall, pinFalls);
    }

}
